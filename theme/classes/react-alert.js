import { createGlobalStyle } from 'styled-components'

const ReactAlertStyle = createGlobalStyle`

#__react-alert__ {
  div div div {
    background-color: var(--primary-bold) !important;
    box-shadow: 1px 1px 5px 1px var(--primary) !important;

    svg {
      stroke: var(--background) !important;
    }

    span {
      color: var(--background) !important;
      text-align: center !important;
      font-weight: 700 !important;
      text-transform: capitalize !important;
    }

    button svg {
      stroke: var(--background) !important;
    }
  }
}

`

export default ReactAlertStyle
