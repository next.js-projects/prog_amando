import { createGlobalStyle } from 'styled-components'

import fonts from '../fonts'

const ButtonsGlobalStyle = createGlobalStyle`
  .btn--phanton {
    padding: 2rem;
    font-size: 2rem;
    outline: none;
    border: none;
    cursor: pointer;

    width: 15rem;
    display: flex;
    margin: auto;
    justify-content: center;

    font-family: ${fonts.types.description};
    font-weight: 700;
    border: 2px solid var(--title);

    background-color: var(--title);
    color: var(--background);

    transition: .5s;
  }

  .btn--phanton:hover {
    background-color: var(--background);
    color: var(--title);
    border-radius: 5px;

    box-shadow: 1px 1px 5px var(--title);

    transition: .5s;
  }
`

export default ButtonsGlobalStyle