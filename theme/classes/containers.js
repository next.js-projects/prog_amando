import { createGlobalStyle } from 'styled-components'

const GlobalContainerStyle = createGlobalStyle`

  :root {
    --container-padding: 1.6rem;
    --container-header-height: 5rem;
    --container-header-total-height: calc(var(--container-padding) * 2 + var(--container-header-height));
  }

  .container {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    padding: var(--container-padding);
  }

`

export default GlobalContainerStyle
