import buttons from './buttons'
import containers from './containers'
import react_alert from './react-alert'
import texts from './texts'

export default [
  buttons,
  containers,
  react_alert,
  texts,
]