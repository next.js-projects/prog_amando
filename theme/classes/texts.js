import { createGlobalStyle } from 'styled-components'
import fonts from '../fonts'

import breakpoints from '../../theme/variables/breakpoints'

const GlobalTextsStyle = createGlobalStyle`

  :root {
    font-size: 60%;
    color: var(--primary);
    font-family: ${fonts.types.description};
  }

  @media (min-width: ${breakpoints.desktop}) {
    :root {
      font-size: 62.5%;
    }
  }

  body {
    font-size: 1.4rem;
  }

  .title {
    font-family: ${fonts.types.title};
  }

  a {
    text-decoration: none;
  }

`

export default GlobalTextsStyle
