import { createGlobalStyle } from "styled-components";
import fontsConfig from "./fontsConfig";

const names = (() => {
  let auxNames = {};

  fontsConfig.forEach(font => {
    auxNames[font.name.toLowerCase()] = font.name;
  });

  return auxNames;
})();

const getAllParams = (params) => {
  if(!params) return ""

  const paramsKeys = Object.keys(params)
  return paramsKeys.map(pk => pk + ": " + params[pk] + ";").join(" ")
}

let types = {}
const createType = (font) => {if(font.type) types[font.type] = font.name }
fontsConfig.forEach(font => createType(font))

const faces = 
  createGlobalStyle`
  ${fontsConfig
  .map(
    font => `@font-face { 
      font-family: ${font.name}; 
      src: url(${font.url}); 
      ${getAllParams(font.params)}
    }`
  )
  .join(" ")}`

export default {
  names,
  types,
  Faces: faces,
};
