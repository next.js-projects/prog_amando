import fm_regular from './Fira_Mono/FiraMono-Regular.ttf'
import fm_medium from './Fira_Mono/FiraMono-Medium.ttf'
import fm_bold from './Fira_Mono/FiraMono-Bold.ttf'

import m_regular from './Merriweather/Merriweather-Regular.ttf'

/*
  To add any font into the project:

  {
    name: "Name of Font",
    url: "font url imported",
    type: "kind of font (title, description...)",
    params: {
      font-weight: normal;
      font-style: normal;
    }
  }
  
*/
export default [

  // Fira Mono Family
  {
    name: "Fira Mono",
    url: fm_regular,
    type: "description",
  },
  {
    name: "Fira Mono",
    url: fm_medium,
    params: {'font-weight': 'bold'}
  },
  {
    name: "Fira Mono",
    url: fm_bold,
    params: {'font-weight': '700'}
  },

  // Merriweather Family
  {
    name: "Merriweather",
    url: m_regular,
    type: "title",
  },
];
