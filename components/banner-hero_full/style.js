import styled from 'styled-components'

export default styled.section`

.overlay {
  background-color: var(--background);
  opacity: 0.85;
}

 .banner-hero {
    min-height: calc(100vh - (var(--container-padding) + var(--container-header-height)));
 }

`