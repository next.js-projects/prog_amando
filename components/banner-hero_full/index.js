import Style from './style'

import BannerHero from '../banner-hero'

export default (props/* = {imageUrl, bannerHeroStyle, overlayStyle, className, children}*/) => {
  return (
    <Style>
      <BannerHero {...props} />
    </Style>
  )
}