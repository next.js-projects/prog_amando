import React, { memo, useCallback } from 'react'

// Style Components
import GlobalStyle from "../theme/globalStyle";
import ClassesGlobalStyles from "../theme/classes"
import FontGlobal from '../theme/fonts'

const DefaultStyles = () => {

  const createAllStyles = useCallback(() => {
    return (
      <>
        { ClassesGlobalStyles.map((ClassesGlobalStyle, index) => <ClassesGlobalStyle key={index} />) }
        <GlobalStyle />
        <FontGlobal.Faces /> 
      </>
    )
  }, [])

  return (
    <>
      {createAllStyles()}
    </>
  )
}

export default memo(DefaultStyles) 