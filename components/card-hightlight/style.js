import styled from 'styled-components'

import breakpoints from '../../theme/variables/breakpoints'

export default styled.article`

  --header-padding: 2rem;
  --header-title: 2rem;
  --header-height: calc(var(--header-padding) + var(--header-title));

  margin: 2rem 0;
  height: 20rem;  

  @media (min-width: ${breakpoints.desktop}) {
    height: 40rem;
  }

  overflow: hidden;

  border: 1px solid var(--secundary);

  transition: .3s;

  &&:hover {
    box-shadow: 0px 0px 5px 1px var(--title);
                // 0px 0px 500px 300px var(--title);
    position: relative;
    z-index: 15;

    .card-thumbnail {
      height: calc(100% + var(--header-height));
      width: auto;
      max-width: none;
    }

    .card-header {
      padding: 0;
      height: 0;
    }
  }

  .card-header {
    padding: var(--header-padding);
    text-align: center;
    background-color: var(--secundary);

    transition: .3s;

    .title {
      font-size: var(--header-title);
      color: var(--primary);
    }
  }

  .card-thumbnail {
    img {
      min-height: 100%;
      min-width: 100%;
    }

    transition: .3s;
  }

`