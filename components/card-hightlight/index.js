import React from 'react'

import Link from 'next/link'

import Style from './style'

import { getColorString } from '../../utils/cssVariableToString'

const CardHightlight = ({card}) => {

  const { title, slug, image } = card.fields
  const { id } = card.sys.id
  const { url: imageUrl, title: imageTitle } = image.fields.file

  return (
    <Style key={id} className="card-hightlight" getColorString={getColorString}>
    <Link href={"/blog/[id]"} as={`/blog/${slug}`}>
      <a>
        <div className="card-header"><h4 className="title">{title}</h4></div>
        <div className="card-thumbnail">
          <img src={imageUrl} alt={imageTitle} title={imageTitle} />
        </div>
        {/* <div className="card-footer"></div> */}
      </a>
    </Link>
    </Style> 
  )

}

export default React.memo(CardHightlight)