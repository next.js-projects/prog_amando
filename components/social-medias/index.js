import React from 'react'

import {
  FaWhatsapp,
  FaLinkedinIn,
} from 'react-icons/fa'

import {
  FiMail
} from 'react-icons/fi'

import Style from './style'

const SocialMedia = ({text}) => {
  
  const config = [
    {
      label: "WhatsApp", 
      Icon: FaWhatsapp, 
      href: `https://api.whatsapp.com/send?phone=${'55011995593220'}&text=${'Olá *Giovanni*, vim através do site *progAmando*, podemos conversar?'}`
    },
    {
      label: "Linkedin", 
      Icon: FaLinkedinIn, 
      href: "https://www.linkedin.com/in/giovanni-pregnolato-rosim-7b709211a/"
    },
    {label: "E-mail", Icon: FiMail, href: 'mailto:giovanni_pr@hotmail.com', content: 'giovanni_pr@hotmail.com'},
  ]



  return (
    <Style className="social-media-group">
      {
        config.map((c, i) => {
          return (
            <a
              className={`social-media ${c.label}`}
              href={c.href} 
              target="_blank"
              alt={c.label} title={c.label}
              key={i}
            >
              <div className="icon-wraper"><c.Icon /></div>
              {c.content && <p>{c.content}</p>}
            </a>
          )
        })
      }
      {text && <p>{text}</p>}
    </Style>
  )

}

export default SocialMedia