import styled from 'styled-components'

export default styled.section`

  display: flex;
  flex-wrap: wrap-reverse;
  justify-content: center;

  margin: 1rem;

  .icon-wraper {
    width: 5rem;
    height: 5rem;
    border-radius: 50%;
    background: white;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  svg {
    width: 3rem;
    height: 3rem;
    background: white;
    color: red;
  }

  a {
    margin: 1rem 1rem;
  }

  a.E-mail {
    display: flex;
    justify-content: center;
    align-items: center;

    p {
      pointer-events: none !important;
      cursor: text;
    }
  }

  a.WhatsApp {
    .icon-wraper {
      background: #4bec68;
    }

    svg {
      color: white;
      background: #4bec68;
    }
    
  }

  a.WhatsApp:hover .icon-wraper {
    box-shadow: 0px 0px 5px 1px #4bec68aa;
  }

  a.Linkedin {
    .icon-wraper {
      background: #0077b5;
    }

    svg {
      color: white;
      background: #0077b5;
    }
  }

  a.Linkedin:hover .icon-wraper {
    box-shadow: 0px 0px 5px 1px #0077b5aa;
  }

  a.E-mail {
    .icon-wraper {
      background: #bab8b7;
      z-index: 1;
    }

    svg {
      color: white;
      background: #bab8b7;
    }

    p {
      padding: 0 1rem;
      color: white;
      background: gray;
      padding: 1rem 1rem 1rem 2rem;
      border-radius: 5px;
      position: relative;
      left: -10px;
    }
  }

  a.E-mail:hover .icon-wraper {
    box-shadow: 0px 0px 5px 1px #bab8b7aa;
  }
  
`