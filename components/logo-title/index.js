import Style from './style'

import Logo from '../logo'

const LogoTitle = ({title}) => {

  return (
    <Style className="logo-title">
      <Logo />
      <div className="title-container">
        <h2 className="title">{title}</h2>
      </div>
    </Style>
  )

}

export default LogoTitle