import React, { useCallback } from 'react'

// Components
import Navbar from '../navbar'
import Head from '../head'

import Style from './style'

const UpperSection = ({navbar, head, bannerHero: {Component, props}}) => {

  const createComponent = useCallback(() => <Component {...props}/>, [])

  return (
    <Style>
      <Head {...head} />
      <Navbar {...navbar} />
      { Component && createComponent() }
    </Style>
  )

}

export default UpperSection