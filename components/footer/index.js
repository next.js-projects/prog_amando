import React from 'react'
import Link from 'next/link'

import Style from './style'

const Footer = () => {

  return(
    <Style>
      <p>Desenvolvido por <a className="giovanni-link" href='https://gdev.netlify.app/home' target='_blank'>Giovanni Pregnolato Rosim</a></p>
    </Style>
  )

}

export default Footer