import styled from 'styled-components'

export default styled.footer`

padding: 2rem;
text-align: center;
background: var(--primary-bold);
box-shadow: 0 -1px 5px 1px var(--primary);

color: var(--background);

a.giovanni-link {
  text-decoration: none;
  color: var(--background);
  font-weight: 700;
  text-decoration: underline;

  transition: .5s;
}

a.giovanni-link: hover {
  color: var(--primary);
  text-decoration: none;
}

`