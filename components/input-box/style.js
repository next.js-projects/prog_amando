import styled from 'styled-components'

import fonts from '../../theme/fonts'

// Reference:
// https://www.youtube.com/watch?v=wgyvLfXBGdQ&ab_channel=OnlineTutorials

export default styled.div`

  margin: 5rem 0;

  position: relative;

  input, textarea {
    position: relative;
    width: 100%;
    height: 100%;
    background: transparent;
    box-shadow: none;
    border: none;
    outline: none;
    height: 5rem;
    border-radius: 5px;

    font-family: ${fonts.types.description} !important;
    color: var(--background);
    font-size: 1.6rem;
    padding: 0 1rem;
    z-index: 1;
  }

  textarea {
    resize: none;
    min-height: 10rem;
    padding: 1rem;
    position: relative;
  }

  input:focus, input:valid, 
  textarea:focus, textarea:valid {
    background: transparent;
    border-bottom: 1px solid var(--title);
    border-left: 1px solid var(--title);
    
    transition: .3s;
  }

  .text {
    position: absolute;
    top: 0;
    left: 0;
    line-height: 4rem;
    font-size: 1.8rem;
    padding: 0 1rem;
    display: block;
    pointer-events: none;

    transition: .3s;
  }

  input:focus + .text,
  input:valid + .text,
  textarea:focus + .text,
  textarea:valid + .text {
    top: -3.5rem;
    left: -1rem;
  }

  .text {
    color: var(--primary-bold);
    font-weight: 700;

    transition: .3s;
  }

  .line {
    position: absolute;
    bottom: 0;
    display: block;
    width: 100%;
    height: 2px;
  
    border-radius: 5px;
    pointer-events: none;
    
    background: var(--title);
    transition: .3s;
  }

  input:focus ~ .line,
  input:valid ~ .line,
  textarea:focus ~ .line,
  textarea:valid ~ .line {
    height: 100%;
  }

`