import React from 'react'

import Style from './style'

const InputBox = ({name, children, ...rest}) => {

  return (
    <Style className="input-box">
      {children}
      <span className="text">{name}</span>
      <span className="line" />
    </Style>
  )

}

export default InputBox