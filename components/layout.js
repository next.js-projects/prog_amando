import { useEffect, useCallback } from 'react'

// Style Components
import DefaultStyles from "./defaultStyles"

import { positions, transitions, Provider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";

const alertOptions = {
  position: positions.TOP_CENTER,
  timeout: 3000,
  offset: '1rem',
  transition: transitions.SCALE
}

const Layout = ({ children }) => {

  useEffect(() => {
    const nextBtn = document.querySelector('#__next-prerender-indicator')
    nextBtn && nextBtn.remove()
  }, [])

  const createChildren = useCallback(() => children, [children])

  return (
    <>
    <Provider template={AlertTemplate} {...alertOptions}>
      { createChildren() }
      <DefaultStyles />
    </Provider>
    </>
  )
}

export default Layout
