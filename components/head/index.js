import NextHead from 'next/head'
import { hotjar } from 'react-hotjar'


const Head = ({
  title = "progAmando", 
  description = "Blog sobre o dia-a-dia de um  desenvolvedor web",
  previewImage,
}) => {

  React.useEffect(() => hotjar.initialize(2034055, 6), [])

  return (
    <NextHead>
      <title>{title}</title>

      <link rel="icon" type="image/png" sizes="96x96" href="/favicons/favicon-96x96.png"></link>

      <meta property="og:title" content={title} key={title} />
      <meta property="og:site_name" content="progAmando" key="ogsitename" />
      <meta name="description" content={description} />
      <meta property="og:description" content={description} key="ogdesc" />
      <meta property="og:image" content={previewImage} key="ogimage" />

      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta charSet="utf-8" />

      {/* <!-- Hotjar Tracking Code for https://prog-amando.vercel.app/ --> */}
      {/* <script>
        {
          (function(h,o,t,j,a,r){
              h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
              h._hjSettings={hjid:2034055,hjsv:6};
              a=o.getElementsByTagName('head')[0];
              r=o.createElement('script');r.async=1;
              r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
              a.appendChild(r);
          })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=')
        }
      </script> */}
    </NextHead>
  )

}

export default Head