import styled from 'styled-components'

export default styled.main`

  overflow: hidden;

  h3 {
    margin: 3.2rem 0;
  }

  .code {
    color: var(--primary-bold);
  }

  .code.align-right {
    text-align: right;
  } 

`