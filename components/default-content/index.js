import React from 'react'

import MainStyle from './style'

import Footer from '../footer'

import Loading from '../loading'

const DefaultContent = ({className, children, headerTxt, footerTxt, isLoading}) => {

  return (
    <>
    <MainStyle className={`main-content ${className}`}>
      <section className="container main-section">
        <Loading forceShow={isLoading} />
        {headerTxt && <h3 className="code">{headerTxt}</h3>}
        {children}
        {footerTxt && <h3 className="code align-right">{footerTxt}</h3>}
      </section>
    </MainStyle>
    <Footer />
    </>
  )

}

export default React.memo(DefaultContent)