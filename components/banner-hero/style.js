import styled from 'styled-components'

import breakpoints from '../../theme/variables/breakpoints'
import fonts from '../../theme/fonts'

export default styled.section`
    position: relative;
    left: 0;
    padding: 5rem 0;
    width: 100vw;
    z-index: 1;

    background-attachment: fixed;

    background-size: cover;
    background-image: url(${props => props.image});

    @media (min-width: ${breakpoints.desktop}) {
      width: calc(100vw - var(--container-padding));
      padding: 10rem 0;
    }

    .overlay {
      height: 100%;
      width: inherit;
      position: absolute;
      top: 0;
      z-index: -1;
    }

    :before {
      content: '';
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      background: inherit;
       /*grayscale for background image*/
      -webkit-filter: grayscale(1); 
      -webkit-filter: grayscale(100%); 
      -moz-filter: grayscale(100%);
      filter: gray; 
      filter: grayscale(100%);
      z-index: -1;
    }
    
`