import Style from './style'

export default ({imageUrl, bannerHeroStyle, overlayStyle, className="", children}) => {

  return (
    <Style className={`banner-hero ${className}`.trim()} style={bannerHeroStyle} image={imageUrl}>
      <div className="overlay" style={overlayStyle} />
      {children}
    </Style>
  )
}