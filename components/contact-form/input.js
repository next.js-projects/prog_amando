import React, { useRef, useEffect } from 'react'
import { useField } from '@unform/core'

const Input = ({name, ...rest}) => {
  const inputRef = useRef()
  const { fieldName, registerField, defaultValue, error, clearError } = useField(name)

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value' // input field name
    })
  }, [fieldName, registerField])

  return (
    <input name={name} ref={inputRef} onFocus={clearError} defaultValue={defaultValue} {...rest} />
  )

}

export default Input