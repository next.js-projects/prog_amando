import React, { useRef, useEffect } from 'react'
import { useField } from '@unform/core'

const TextArea = ({name, ...rest}) => {
  const textAreaRef = useRef()
  const { fieldName, registerField, defaultValue, error, clearError } = useField(name)

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: textAreaRef.current,
      path: 'value' // input field name
    })
  }, [fieldName, registerField])

  return (
    <textarea name={name} ref={textAreaRef} onFocus={clearError} defaultValue={defaultValue} {...rest} />
  )

}

export default TextArea