import React, { useRef } from 'react'

// Components
import { Form } from '@unform/web'
import Input from './input'
import InputBox from '../input-box'
import Textarea from './text-area'

import PhantonButton from '../../components/phanton-btn'

import SocialMedias from '../../components/social-medias'

const ContactForm = () => {
  const sectionContactFormRef = useRef()
  const formRef = useRef()

  const emailStatus = {
    NONE: 'none',
    SENDING: 'sending',
    SUCCESS: 'success',
    ERROR: 'error',
  }

  const handleSubmitForm = (data, { reset }, event) => {

    const configError = [
      {fieldName: 'name', errorMsg: 'Preencha o nome'},
      {fieldName: 'email', errorMsg: 'Preencha o email'},
      {fieldName: 'message', errorMsg: 'Por favor, digite uma mensagem...'},
    ]

    let errorObj = {}
    let hasError = false
    configError.forEach(err => {
      if(data[err.fieldName] !== "") return

      errorObj[err.fieldName] = err.errorMsg
      hasError = true
    })

    if(hasError) {
      formRef.current.setErrors(errorObj)
      return
    }

    var url = "https://formspree.io/xwkwpdro"

    const tagsName = document.querySelectorAll('.user-name')
    tagsName.forEach(n => n.innerHTML = data['name'])

    ajax('POST', url, new FormData(event.target), 
    () => { changeContactFormStatus(emailStatus.SUCCESS) }, 
    () => { changeContactFormStatus(emailStatus.ERROR) })

    changeContactFormStatus(emailStatus.SENDING)
  }

  const createForm = () => {
    return (
      <Form 
        action="https://formspree.io/f/xwkwpdro"
        method="POST"
        ref={formRef} 
        initialData={initialData} 
        onSubmit={handleSubmitForm}
        className="email-form"
      >

        <InputBox name="Nome">
          <Input name="name" required="required" />
        </InputBox>

        <InputBox name="Email">
          <Input name="email" type="text" required="required" />
        </InputBox>
        
        <InputBox name="Mensagem">
          <Textarea name="message" required="required"
            onChange={() => event.target.style.height = event.target.scrollHeight + "px"} />
        </InputBox>

        <button type="submit">
          <PhantonButton 
            className="btn--submit-form"
            linkProps={{href: "#"}}
            label="Enviar"
          />
        </button>

      </Form>
    )
  }

  const changeContactFormStatus = (status) => {
    sectionContactFormRef.current.classList = 
    sectionContactFormRef.current.classList.value.replace(
      /form-status--(\w)+/g, 
      "form-status--"+status
    )
  }

  const createSuccessMessage = () => {
    return (
      <div className="email-status email-success">
        <p><span className="user-name" />, seu e-mail foi enviado com sucesso!</p>
        <img src="/assets/icons/check-icon.png" />
        <p>Muito obrigado por entrar em contato.</p>
      </div>
    )
  }

  const createErrorMessage = () => {
    return (
      <div className="email-status email-error">
        <p>Ocorreu algum problema no envio do email!</p>
        <img src="/assets/icons/icon_alert.png" />
        <p>Desculpe-me <span className="user-name" /> o transtorno, pode me enviar uma mensagem por uma das minhas redes...</p>
        <SocialMedias />
      </div>
    )
  }

  const createSendingEmail = () => {
    return (
      <div className="email-status email-sending">
        <p><span className="user-name" />, seu email já está a caminho...</p>
        <img src="/assets/icons/sending.gif" />
      </div>
    )
  }

  const ajax = (method, url, data, success, error) => {
    var xhr = new XMLHttpRequest()
    xhr.open(method, url)
    xhr.setRequestHeader("Accept", "application/json")
    xhr.onreadystatechange = function() {
      if (xhr.readyState !== XMLHttpRequest.DONE) return
      if (xhr.status === 200) success(xhr.response, xhr.responseType)
      else error(xhr.status, xhr.response, xhr.responseType)
    }
    xhr.send(data)
  }

  const initialData = {
    // name: "Giovanni",
    // email: "giovanni_pr@hotmail.com",
    // password: "123"
  }

  return (
    <section ref={sectionContactFormRef} className="contact-form form-status--none">
      {createForm()}
      {createSuccessMessage()}
      {createErrorMessage()}
      {createSendingEmail()}
    </section>
  )

}

export default ContactForm