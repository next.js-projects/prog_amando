import React, { useCallback, useRef, useEffect } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'

import Style, { MenuDesktop, MenuMobile } from './style'

import ThemeSwitcher from '../../theme-switcher'

const menuLinks = [
  {label: "Blog", href: "/"},
  {label: "Contato", href: "/contato"},
  {label: "Sobre", href: "/sobre"},
]

const Menu = () => {

  const menuMobile = useRef()
  const toggleShowMenu = useCallback(() => menuMobile.current.classList.toggle("open"), [])

  const createListOfLinks = useCallback((device) => {
    return (
      <ul className="menu-list">
      {menuLinks.map((l, i) => <li key={i} className="menu-item"><Link href={l.href}><a>{l.label}</a></Link></li>)}
      <li className="menu-item menu-item--theme-switcher">
        <ThemeSwitcher className={`switcher-menu--${device}`} />
      </li>
      </ul>
    )},
  [])

  const router = useRouter()

  useEffect(() => {
    const path = router.pathname
    const id = menuLinks.findIndex(l => l.href === path)
    if(id < 0) return

    document.querySelectorAll(".nav-desktop li")[id].classList.add("selected")
    document.querySelectorAll(".nav-mobile li")[id].classList.add("selected")
  }, [])

  return (
    <Style>
      <MenuDesktop className="nav-desktop">
      {createListOfLinks('desktop')}
      </MenuDesktop>

      <MenuMobile className="nav-mobile" /*onClick={toggleShowMenu}*/ ref={menuMobile}>
        <div className="hamburger-menu" onClick={toggleShowMenu}>
          <div className="menu-line" />
          <div className="menu-line" />
          <div className="menu-line" />
        </div>
        {createListOfLinks('mobile')}
      </MenuMobile>
    </Style>
  )

}

export default Menu