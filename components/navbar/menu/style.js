import styled from 'styled-components'

import breakpoints from '../../../theme/variables/breakpoints'

export default styled.section`

  a:hover, .menu-item.selected a {
    color: var(--subtitle);
  }

  .menu-item.selected a {
    cursor: default;
    opacity: .5;
    pointer-events: none;
  }

  .nav-mobile .menu-item.selected a {
    opacity: 1;
  }

  a {
    text-decoration: none;
    font-weight: 700;
  }

  @media (min-width: ${breakpoints.desktop}) {
    nav.nav-desktop {
      display: block;
    }

    nav.nav-mobile {
      display: none;
    }
  }

`

export const MenuMobile = styled.nav`

  .hamburger-menu {
    cursor: pointer;
    width: 5rem;
  }

  .hamburger-menu .menu-line {
    position: relative;

    border: 1px solid var(--primary-bold);
    border-bottom-width: 3px;
    border-left-width: 1rem;

    max-width: 3rem;
    border-radius: 5px;
  }

  .hamburger-menu:hover .menu-line {
    border-color: var(--subtitle);
  }

  && .hamburger-menu {
    .menu-line:nth-child(1) {
      transform: rotate(0);
      transition: .3s;
    }
    .menu-line:nth-child(2) {
      transform: rotate(0);
      transition: .3s;
    }
    .menu-line:nth-child(3) {
      opacity: 1; transition: .3s;
    }
  }

  &&.open .hamburger-menu {
    .menu-line:nth-child(1) {
      transform: rotate(45deg);
      top: 4px; transition: .3s;
    }
    .menu-line:nth-child(2) {
      transform: rotate(-45deg);
      top: -3px; transition: .3s;
    }
    .menu-line:nth-child(3) {
      opacity: 0; transition: .3s;
    }
  }

  .hamburger-menu .menu-line + .menu-line {
    margin-top: 3px;
  }

  &&.open .menu-list {
    visibility: visible;
    opacity: 1;

    transition: .3s;
  }

  .menu-list {
    width: 100%;
    left: 0;
    top: calc(var(--height) + var(--container-padding) * 2);
    text-align: center;
    font-size: 2rem;
    background-color: var(--primary-bold);
    padding: 3rem 0;
    z-index: 100;

    position: absolute;
    visibility: hidden;
    opacity: 0;

    color: var(--title);

    transition: .3s;
  }

  .menu-list a {
    color: var(--background);

    transition: .3s
  }

  .menu-list a:hover {
    color: var(--subtitle);
  }

  .menu-list .menu-item + .menu-item {
    margin-top: 2rem;
  }

  .menu-item--theme-switcher {
    height: 8rem;  
  }

  .menu-item--theme-switcher {
    
    small {
      color: var(--background);
    }

    .theme-switcher--select {
      background-color: var(--background);
      padding: 1rem;
    }

    .theme-switcher {
      width: 80%;
      max-width: 30rem;
      margin-left: auto;
      margin-right: auto;
    }

  }

`

export const MenuDesktop = styled.nav`

  display: none;

  .menu-list {
    display: flex;
    align-items: center;
  }

  a:hover {
    color: var(--subtitle);
  }

  a {
    color: var(--primary-bold);
    transition: .3s
  }

  .menu-item + .menu-item {
    margin-left: 1.6rem;
  }

  .menu-item--theme-switcher {
    height: 8rem;

    small {
      font-size: 1rem;
    }

    .theme-switcher--header .theme-switcher--select {
      border: 1px solid var(--primary);
      border-radius: 5px;
      padding: .5rem;

      transition: .3s;
    }

    .theme-switcher--header.show .theme-switcher--select {
      border-radius: 5px 5px 0 0;
    }

    .theme-switcher-list {
      border: 1px solid var(--primary);
    }

  }

`