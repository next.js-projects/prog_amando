import styled from 'styled-components'

export default styled.header`

  position: sticky;
  background-color: var(--background);
  border-bottom: 1px solid var(--primary-bold);
  box-shadow: 1px 3px 5px var(--primary-bold);

  z-index: 10000;
  width: 100%;

  &&.header-position--fixed {
    position: fixed;
    top: calc(var(--container-header-total-height) * -1);
    transition: .3s;
  }

  &&.header-position--fixed.show {
    position: fixed;
    top: 0;
    transition: .3s;
  }

  .header-container {
    display: flex;
    justify-content: space-between;

    --height: var(--container-header-height);
    --after-width: 2rem;
    --after-heigth: calc(var(--height) - var(--container-padding) / 2);
  }

  .header-element {
    height: var(--height);
    width: calc(50% - var(--after-width));

    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .triangle {
    position: relative;
    width: 0;
    height: 0;
    // top: calc(var(--container-padding) * -1);

    border-top: var(--after-heigth) solid var(--primary-bold);
    border-left: var(--after-width) solid  transparent;
    border-bottom: var(--after-heigth) solid var(--primary-bold);
  }

  .triangle-left {
    left: calc(100% + 1px);
  }

  .triangle-right {
    right: var(--after-width);
    transform: rotateZ(180deg);
  }

  picture {
    height: var(--height);
    width: var(--height);
    // background-color: white;
    position: absolute;
  }


`