import React, {useRef, useEffect, useCallback} from 'react'
import Link from 'next/link'

import Style from './style'

// Components
import Menu from './menu'
import Logo from '../logo'

const Header = (props) => {
  
  useEffect(() => {
    headerBehaviour()
  }, [])

  const headerBehaviour = () => {
    
    document.header = document.querySelector('header')
    document.headerClass = ""

    const headerSize = document.header.clientHeight  

    const rootEl = document.querySelector(":root")

    if(document.scrollBehaviour) document.removeEventListener('scroll', document.scrollBehaviour)

    document.scrollBehaviour = () => {
      if(rootEl.lastScrollTop > rootEl.scrollTop) { //scroll up
        if(rootEl.scrollTop > 0) setHeaderStyle("header-position--fixed show")
        else setHeaderStyle("")
      }
      else { // scroll down
        if(rootEl.scrollTop > headerSize) setHeaderStyle("header-position--fixed")
        else setHeaderStyle("")
      }
      rootEl.lastScrollTop = rootEl.scrollTop
    }

    document.addEventListener('scroll', document.scrollBehaviour)

  }

  const setHeaderStyle = (name) => {

    if(document.headerClass == name) return

    const classList = document.header.classList
    const [ style1, style2 ] = classList

    document.header.classList.value = [style1, style2, name].join(" ")

    document.headerClass = name
  }

  return (
    <Style>
      <section className="container header-container">
        <div className="header-element header-left">
          <div className="triangle triangle-left"/>

          <picture className={props.className}>
            <Link href="/"><a><Logo /></a></Link>
          </picture>

        </div>
        <div className="header-element header-right">
          <div className="triangle triangle-right"/>

          <Menu />
          
        </div>
      </section>
    </Style>
  )

}

export default Header