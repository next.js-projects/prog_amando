import Style from './style'

const Logo = () => {

  return (
    <Style className="logo">
      <img className='heart' src='/assets/images/logo/heart.svg' alt="progAmando Logo" title="progAmando" />
      <img className='bits' src='/assets/images/logo/layer-01.png' alt="progAmando Logo" title="progAmando" />
    </Style>
  )

}

export default Logo