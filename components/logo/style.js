import styled from 'styled-components'

export default styled.div`

  cursor: pointer;

  transition: .5s; 

  .heart {
    filter: var(--filter-primary) drop-shadow(1px 0px 5px var(--primary));
    transition: .5s;  
  }

  &&:hover .heart {
    filter: var(--filter-primary) drop-shadow(1px 0px 15px var(--primary));
  }

  &&:hover {
    transform: scale(.8);
  }

  .bits {
    position: absolute;
    left: 0;
    z-index: 1;
    filter: var(--filter-title);
  }

`