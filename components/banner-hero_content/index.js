import Style from './style'

import htmlReactParser from 'html-react-parser'

import BannerHero from '../banner-hero'
import LogoTitle from '../logo-title'

const BannerHeroContent = ({
  imageUrl, bannerHeroStyle, overlayStyle, className="", children,
  title, paragraphs, insideImage
}) => {
  return (
    <Style>
      <BannerHero {...{imageUrl, bannerHeroStyle, overlayStyle, className, children,}}  >
        <div className="container">
          <div className="banner-description">
            <h1>{htmlReactParser(title)}</h1>
            { paragraphs.map((p, i) => <p key={i}>{p}</p>) }
          </div>
          <div className="banner-image">
            <LogoTitle { ...insideImage } />
          </div>
        </div>
        {children}
      </BannerHero>
    </Style>
  )
}

export default BannerHeroContent