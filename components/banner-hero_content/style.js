import styled from 'styled-components'

import fonts from '../../theme/fonts'

import breakpoints from '../../theme/variables/breakpoints'

export default styled.section`

    .container {
      display: grid;
      grid-gap: 2rem;
      height: 100%;
      align-items: center;
      justify-items: center;

      text-align: center;
    }

    .banner-image {
      grid-row-end: -1;
    }
    
    @media (min-width: ${breakpoints.desktop}) {
      .container {
        grid-template-columns: 1fr 1fr;
      }

      .banner-image {
        grid-row-end: auto;
      }
    }

    h1 {
      margin-bottom: 4rem;
      font-size: 3rem;
      font-family: ${fonts.types.title};
    }

    p {
      font-weight: 700;
    }

    p + p {
      margin-top: 1rem;
    }

    h1 span {
      color: var(--title);
    }

    // Blog
    .overlay {
      background-color: var(--background);
      opacity: 0.75;
    }

    .logo-title {
      height: 40vh;

      .logo {
        width: inherit;
        height: inherit;

        animation: pulse-shadow 2s infinite;
        animation-direction: alternate;
      }

      @keyframes pulse-shadow {
        0% {
          filter: drop-shadow(1px 0px 5px var(--primary));
        }
        100% {
          filter: drop-shadow(1px 0px 15px var(--primary));
        }
      }

      img {
        width: auto;
        height: inherit;
      }

      .heart {
        position: absolute;
      }

      .bits {
        position: relative;
      }

      .title-container {
        position: relative;
        top: -110px;
        z-index: 1;
        background: var(--background);
        padding: 1rem;
        border-radius: 5px;

        transform: scale(1);
        animation: pulse 2s infinite;
        animation-direction: alternate;
      }

      .title-container:before {
        content: '';
        width: 40px;
        height: 30px;
        border-radius: 5px;
        background: var(--background);
        position: absolute;
        top: 0;
        left: -10px;
      }

      .title-container:after {
        content: '';
        width: 40px;
        height: 30px;
        border-radius: 5px;
        background: var(--background);
        position: absolute;
        top: 0;
        right: -10px;
      }

      transform: scale(1);
      animation: pulse 2s infinite;
      animation-direction: alternate;
    }

    @keyframes pulse {
      0% {
        transform: scale(0.95);
      }
      100% {
        transform: scale(1.05);
      }
    }
`