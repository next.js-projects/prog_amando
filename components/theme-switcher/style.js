import styled from 'styled-components'

export default styled.ul`

  .theme-switcher--header {
    z-index: 1;
    cursor: pointer;
    position: relative;
  }

  .theme-switcher--header .theme-switcher--select {
    display: flex;
    justify-content: space-between;
    align-items: center;

    svg {
      transform: scaleY(1);
      font-size: 2rem;
      transition: .3s;
    }
  }

  .theme-switcher--header.show .theme-switcher--select {
    svg {
      transform: scaleY(-1);
    }
  } 


  .theme-switcher-list {
    position: relative;
    top: -30px;
    visibility: hidden;
    opacity: 0;

    transition: .3s;
  }

  .theme-switcher-list.show {
    top: 0px;
    visibility: visible;
    opacity: 1;
  }

  li {
    display: flex;
    align-items: center;

    padding: 1rem;
    cursor: pointer;

    .theme-name, .theme-color--circle {
      cursor: pointer;
      transition: .3s;
    }
  }

  .theme-name {
    margin-left: 1.6rem;
  }

  li.selected {
    cursor: auto;
    pointer-events: none;

    .theme-name {
      font-weight: 700;
      border-bottom: 2px solid;
      border-radius: 5px;
      cursor: auto;
      pointer-events: none;
    }

    .theme-color--circle {
      transform: scale(1.2);
      cursor: auto;
      pointer-events: none;
    }
  }

  li:hover {
    .theme-name {
      border-bottom: 2px solid;
      border-radius: 5px;
    }
    .theme-color--circle {
      transform: scale(1.2);
    }
  }

  .theme-color--circle {
    width: 20px;
    height: 20px;
    border: 3px solid;
    border-radius: 50%;
  }

`