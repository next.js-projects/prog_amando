import { useContext, useMemo, useEffect } from 'react'
import { ThemeContext } from '../../services/contexts/theme'

import Style from './style'

import { AiOutlineDown } from 'react-icons/ai'

import colors, { getThemesName } from '../../theme/colors'

const ThemeSwitcher = ({className}) => {
  
  const context = useContext(ThemeContext)

  const getThemeNames = useMemo(() => getThemesName(), [])

  useEffect(() => {
    const currentTheme = context.getTheme()
    setTheme(currentTheme, false)
    setCurrentThemeInSelect()
  }, [])

  const setTheme = (themeName, changeThemeContext = true) => {
    const lastSelected = document.querySelector(`.${className} .theme-switcher-list .selected`)
    lastSelected && lastSelected.classList.remove('selected')
    document.querySelector(`.${className} .theme-switcher-list .${themeName}`).classList.toggle('selected')

    changeThemeContext && context.setTheme(themeName)
  }

  const createItemList = (item) => {

    const { title, background, primary, secundary } = colors[item]

    const spanStyle = {
      borderColor: colors[item]['primary-bold'],
      backgroundColor: title,
    }

    const liStyle = {
      backgroundColor: background,
    }

    const labelStyle = {
      color: primary,
      borderColor: primary,
    }

    return (
      <li key={item} style={liStyle} className={item} 
        onClick={(e) => {
          setTheme(item); 
          setCurrentThemeInSelect(e);
          toggleThemeSwitcherList();
        }}
      >
        <span className="theme-color--circle" style={spanStyle} />
        <label className="theme-name" style={labelStyle}>{item}</label>
      </li>
    )
  }

  const toggleThemeSwitcherList = () => {
    document.querySelector(`.${className} .theme-switcher-list`).classList.toggle('show')
    document.querySelector(`.${className} .theme-switcher--header`).classList.toggle('show')
  }

  const setCurrentThemeInSelect = () => {
    const selectedClone = 
      document.querySelector(`.${className} .theme-switcher-list .selected`).cloneNode(true)

    const selectCurrentTheme = 
      document.querySelector(`.${className} .theme-switcher--select .theme-switcher--current-theme`)

    selectCurrentTheme.removeChild(selectCurrentTheme.childNodes[0])
    selectCurrentTheme.appendChild(selectedClone)
    
  }

  return (
    <Style className={`theme-switcher ${className}`}>
      <div className="theme-switcher--header" onClick={toggleThemeSwitcherList}>
        <small>Escolha o tema:</small>
        <div className="theme-switcher--select">
          <div className="theme-switcher--current-theme">tema atual</div>
          <AiOutlineDown />
        </div>
      </div>
      <ul className="theme-switcher-list">
      {
        getThemeNames.map(theme => createItemList(theme))
      }
      </ul>
    </Style>
  )

}

export default ThemeSwitcher