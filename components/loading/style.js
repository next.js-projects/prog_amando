import styled from 'styled-components'

export default styled.div`

  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;

  display: flex;
  justify-content: center;
  align-items: center;

  filter: blur(1px);

  &&.loading {
    opacity: 0;
    z-index: -10;
  }

  &&.loading.loading-show {
    background: var(--background);
    z-index: 1000000;
    opacity: 1;

    transition: .5s;
  }

  &&.loader span {
    width: 20px;
    height: 20px;
    margin: 0 5px;
    border-radius: 20px;
    background: var(--primary);
    box-shadow: inset -5px -5px 5px var(--primary),
                inset 5px 5px 5px var(--primary-bold);
    animation: animate-loading 7s linear infinite;
    animation-delay: calc(.5s * var(--i));
  }

  @keyframes animate-loading {
    0% {
      box-shadow: -6px -6px 10px var(--primary),
      6px 6px 10px var(--primary-bold);
    }
    100% {
      box-shadow: inset -5px -5px 5px var(--primary),
      inset 5px 5px 5px var(--primary-bold);
    }
  }

`