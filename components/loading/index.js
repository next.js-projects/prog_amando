import React, { useEffect, useRef } from 'react'
import Router, { useRouter } from 'next/router'

import Style from './style'

// https://www.youtube.com/watch?v=9VwX-JOYPcY&ab_channel=OnlineTutorials

const Loading = ({forceShow}) => {

  const loadingRef = useRef()
  const router = useRouter()

  const handleStart = () => loadingRef.current.classList.add("loading-show")
  const handleComplete = () => loadingRef.current.classList.remove("loading-show")

  // useEffect(() => {
    
  //   router.events.on('routeChangeStart', handleStart)
  //   router.events.on('routeChangeComplete', handleComplete)

  //   return () => {
  //       router.events.off('routeChangeStart', handleStart)
  //       router.events.off('routeChangeComplete', handleComplete)
  //   }

  // }, [])

  useEffect(() => {

    if(typeof forceShow !== "boolean") return

    forceShow ? handleStart() : handleComplete()

  }, [forceShow])

  return(
    <Style ref={loadingRef} className={`loading loader ${forceShow ? 'loading-show' : ''}`}>
      <span style={{"--i":1}} />
      <span style={{"--i":2}} />
      <span style={{"--i":3}} />
      <span style={{"--i":4}} />
      <span style={{"--i":5}} />
      <span style={{"--i":6}} />
      <span style={{"--i":7}} />
    </Style>
  )

}

export default Loading
