import styled from 'styled-components'

// Reference
/* https://www.youtube.com/watch?v=3RRgVHd2TXQ&ab_channel=OnlineTutorials */

// https://codepen.io/giovanni-pregnolato-rosim/pen/jOrVObO

export default styled.div`

  a {
    position: relative;
    display: inline-block;
    // padding: 25px 30px;
    // margin: 40px 0;
    // color: blueviolet;
    // font-size: 24px;
    text-decoration: none;
    // letter-spacing: 4px;
    
    overflow: hidden;
    -webkit-box-reflect: below 1px linear-gradient(transparent, #0003);
    
    transition .5s;
  }

  a:hover {
    // color: white;
    box-shadow: 0 0 5px var(--title),
                0 0 25px var(--title),
                0 0 50px var(--title),
                0 0 200px var(--title);
  }
  
  a span {
    position: absolute;
    display: block;
  }


  a span:nth-child(1) {
    top: 0;
    left: 0;
    width: 100%;
    height: 2px;
    background: linear-gradient(90deg, transparent, var(--background));
      
     animation: animate1 1s linear infinite;
  }
  
  @keyframes animate1 {
    0% {
      left: -100%;
    }
    50%, 100% {
      left: 100%;
    }
  }
  
  a span:nth-child(2) {
    top: -100%;
    right: 0;
    width: 2px;
    height: 100%;
    background: linear-gradient(180deg, transparent, var(--background));
      
     animation: animate2 1s linear infinite;
     animation-delay: .25s;
  }
  
  @keyframes animate2 {
    0% {
      top: -100%;
    }
    50%, 100% {
      top: 100%;
    }
  }
  
  a span:nth-child(3) {
    right: -100%;
    bottom: 0;
    height: 2px;
    width: 100%;
    background: linear-gradient(270deg, transparent, var(--background));
      
     animation: animate3 1s linear infinite;
     animation-delay: .5s;
  }
  
  @keyframes animate3 {
    0% {
      right: -100%;
    }
    50%, 100% {
      right: 100%;
    }
  }
  
  a span:nth-child(4) {
    bottom: -100%;
    left: 0;
    width: 2px;
    height: 100%;
    background: linear-gradient(360deg, transparent, var(--background));
      
     animation: animate4 1s linear infinite;
     animation-delay: .75s;
  }
  
  @keyframes animate4 {
    0% {
      bottom: -100%;
    }
    50%, 100% {
      bottom: 100%;
    }
  }


`
