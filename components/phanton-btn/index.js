import React from 'react'
import Link from 'next/link'

import Style from './style'

const PhantonButton = ({className = "", linkProps, label}) => {

  return (
    <Style>
    <Link {...linkProps}>
      <a className={`${className} btn--phanton`}>
        <span />
        <span />
        <span />
        <span />      
        {label}
      </a>
    </Link>
    </Style>
  )

}

export default PhantonButton