This is a starter template for [Learn Next.js](https://nextjs.org/learn).

https://pt.stackoverflow.com/questions/282395/como-trabalhar-com-fork-de-um-projeto

No seu clone local do seu repositório que foi feito o fork, você pode adicionar o repositório original do GitHub como remote. remotes são como apelidos para as URLs dos repositórios - por exemplo, origin é um deles. Então, você pode usar git fetch para trazer todas as branches daquele repositório no upstream, e git rebase para continuar a trabalhar na sua versão. A sequência de comandos seria mais ou menos assim:

# Adiciona o remote, chamando-o de "upstream":

git remote add upstream https://github.com/usuario/projeto.git

# Traz todas as branches daquele remote para remote-tracking branches,
# como por exemplo upstream/master:

git fetch upstream

# Garantindo que você esta na branch master:

git checkout master

# Rescreve sua master branch para que quaisquer commits seus que
# ainda não estão na upstream/master sejam replicados no topo daquela
# outra branch

git rebase upstream/master