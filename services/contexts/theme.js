import React, { useEffect, useState, createContext } from 'react'

export const ThemeContext = createContext()

export default ({ children }) => {

  const defaultTheme = 'dark'

  const setTheme = (theme) => {
    localStorage.setItem("theme", theme)
    document.querySelector(':root').classList = theme
  }

  const getTheme = () => 
    localStorage.getItem("theme") || 
    document.querySelector(':root').classList[0] ||
    defaultTheme

  const toggleTheme = () => setTheme(getTheme() === defaultTheme ? 'light' : defaultTheme)

  useEffect(() => setTheme(getTheme()), [])

  return (
    <ThemeContext.Provider value={{toggleTheme, setTheme, getTheme}}>
      {children}
    </ThemeContext.Provider>
  )
}