import styled from 'styled-components'

export default styled.div`
  background-color: var(--background);
  
  .contact-form {
    max-width: 80rem;
    border: 1px solid var(--title);
    border-left: none;
    border-right: none;
    padding: 4rem 5rem 10rem 5rem;
    border-radius: 10px;
    margin: auto;
  }

  .email-form, .email-success, .email-error, .email-sending {
    display: none;
  }

  .form-status--none .email-form, 
  .form-status--sending .email-sending,
  .form-status--success .email-success,
  .form-status--error .email-error {
      display: block;
  }

  .email-status {
    text-align: center;
    font-size: 2rem;

    img { 
      margin: 10rem auto;
      height: 10rem;
      max-width: 100%;
      border-radius: 50%;
    }
  }

  .email-sending img {
    border: 5px solid var(--primary);
  }

  .btn--submit-form {
    padding: 2rem;
    font-size: 2rem;
    outline: none;
    border: none;
    cursor: pointer;

    width: 15rem;
    display: flex;
    margin: auto;
    justify-content: center;

    font-weight: 700;
    border: 2px solid var(--title);

    background-color: var(--title);
    color: var(--background);

    pointer-events: none;

    transition: .5s;
  }

  button[type="submit"]:hover .btn--submit-form {
    background-color: var(--background);
    color: var(--title);
    border-radius: 5px;

    box-shadow: 1px 1px 5px var(--title);

    transition: .5s;
  }

  button[type="submit"] {
    border: none;
    outline: none;
    background: transparent;
    margin: auto;
    display: flex;
  }

  button[type="submit"] div {
    cursor: pointer;
  }


  .social-media-group {
    margin: 5rem 0;
  }

  .user-name {
    color: var(--title);
    text-decoration: underline;
    font-weight: 700;
  }

`