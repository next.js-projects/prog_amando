import Style from './style'

// Components
import UpperSection from '../../components/upperSection'
import BannerHeroContent from '../../components/banner-hero_content'
import DefaultContent from '../../components/default-content'

import ContactForm from '../../components/contact-form'

// https://youtu.be/P65RJTTqkN4
// https://unform.dev/

const Contact = () => {
  return (
    <Style>
      <UpperSection
        navbar={{
          className: "contact"
        }}
        head={{
          title: "progAmando - Contato", 
          description: "Ficou com alguma dúvida? Ou talvez, tenha uma sugestão à fazer? Não deixe de enviar sua mensagem...",
          previewImage: '/assets/images/banners/contact.jpg',
        }}
        bannerHero={{
          Component: BannerHeroContent,
          props: {
            imageUrl: "/assets/images/banners/contact.jpg",
            // bannerHeroStyle, 
            // overlayStyle, 
            className: "contact",
            title: "<span>prog</span>Amando",
            paragraphs: [
              'Ficou com alguma dúvida?',
              'Ou talvez, tenha uma sugestão à fazer?',
              'Não deixe de enviar sua mensagem...'
            ],
            insideImage: {
              title:"Contato"
            },
          }
        }}
      />
      <DefaultContent className="contact" 
        headerTxt="<form method='POST'>"
        footerTxt="</form>"
      >
        <ContactForm />
      </DefaultContent>
    </Style>
  )
}

export default Contact