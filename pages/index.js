import { useEffect, useCallback, useMemo, useState } from 'react'

// Contenful
import { createClient } from "contentful"
import config from "../config.json"

import Style from './style'

// Components
import UpperSection from '../components/upperSection'
import BannerHeroContent from '../components/banner-hero_content'
import DefaultContent from '../components/default-content'
import CardHightlight from '../components/card-hightlight'

// Instantiate the app client
const client = createClient({
  space: config.space,
  accessToken: config.accessToken
})

const CardList = React.memo(({allPosts}) => (
  <section className="blog-post-list">
    {allPosts.map(p => <CardHightlight card={p} key={p.sys.id} />)}
  </section>
))

const Blog = () => {

  const [allPosts, setAllPosts] = useState([])

  const getPosts = useCallback(() => {
    client.getEntries({
      content_type: "progAmandoArticle",
      select: ["fields.title", "fields.slug", "fields.image", "fields.tagList"],
      order: "-sys.createdAt"
    }).then(entries => setAllPosts(entries.items))
  })

  useEffect(() => getPosts(), [])

  const bannerParams = useMemo(() => {
    return ({
      Component: BannerHeroContent,
      props: {
        imageUrl: "/assets/images/banners/blog.jpg",
        // bannerHeroStyle, 
        // overlayStyle, 
        className: "blog",
        title: "<span>prog</span>Amando",
        paragraphs: [
          'Seu blog sobre as novidades, curiosidades',
          'e dia-a-dia da vida de um programador.'
        ],
        insideImage: {
          title:"Blog"
        },
      }
    })
  }, [])

  const headParams = useMemo(() => {
    return ({
      previewImage: '/assets/images/banners/blog.jpg'
    })
  }, [])

  return (
    <Style>
      <UpperSection head={headParams} bannerHero={bannerParams} />
      <DefaultContent className="blog"
        headerTxt="<main id='blog'>"
        footerTxt="</main>"
        isLoading={!allPosts[0]}
      >
        <CardList allPosts={allPosts}/>
      </DefaultContent>
    </Style>
  )
}

export default Blog