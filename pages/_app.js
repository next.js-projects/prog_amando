import Layout from '../components/layout'

import ThemeContextProvider from '../services/contexts/theme'

// TODOs:
// * Pequenos Components e funcionalidades
// 2- Porcentagem de leitura
// 3- Google Adsense
// 5- Thumbnail Serveless (https://www.youtube.com/watch?v=qvetoR6V5ic&list=PLej88uj_MJ2HqBYA8nMxgOyx_ls8lGb3E&index=13)

// * Funcionalidades futuras (com relação ao crescimento do blog)
// 1- Busca de conteúdos, referencia por tags
// 2- Conteúdos relacionados
// 3- Google Analicts
// 4- Offline First (https://www.youtube.com/watch?v=EX48Yk_e2OE&list=PLej88uj_MJ2HqBYA8nMxgOyx_ls8lGb3E&index=5&t=0s)

function App({ Component, pageProps }) {
  return (
    <ThemeContextProvider>
      <Layout><Component {...pageProps} /></Layout>
    </ThemeContextProvider>
  )
}

export default App