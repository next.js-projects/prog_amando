import Style from './style'

import htmlReactParser from 'html-react-parser'

// Components
import UpperSection from '../../components/upperSection'
import BannerHeroFull from '../../components/banner-hero_full'
import DefaultContent from '../../components/default-content'
import SocialMedias from '../../components/social-medias'

import PhantonButton from '../../components/phanton-btn'

export default () => {

  const aboutMeParagraphs = [
    "Sou <strong>Giovanni Pregnolato Rosim</strong>, formado em <strong>Jogos Digitais</strong> na <strong>FATEC</strong>.",
    "Sempre fui curioso para entender como os jogos eram feitos e conheci na programação a melhor forma.",
    "A área web, com o tempo foi ganhando um lugar no meu coração, começando a dominar meu tempo de estudo e foco de carreira.",
    "Com algumas experiências e conhecimentos adquiridos dentro do desenvolvimento, sinto que fazer o blog do <strong>progAmando</strong> foi um caminho natural, para uma pessoa que adora compartilhar aprendizado e estimular o crescimento de quem me acompanha.",
  ]

  const content = () => {
    return (
      <DefaultContent className="about"
        headerTxt="<main id='about'>"
        footerTxt="</main>"
      >
        <section className="container about-container">
          <section className="about-infos">

            <h4 className="subtitle">Um pouco sobre mim...</h4>
            
            <section className="about-me">
              {aboutMeParagraphs.map((p,i) => <p key={i}>{htmlReactParser(p)}</p>)}
            </section>

            <PhantonButton 
              className="btn-contact btn-mobile"
              linkProps={{href: "/contato"}}
              label="Entre em contato"
            />

            <SocialMedias text="... ou se preferir, através das minhas redes sociais:" />
          
          </section>
          
          <section className="about-photo">
            <picture>
              <img src="/assets/images/me.png" title="Giovanni" alt="Giovanni"/>
            </picture>

            <PhantonButton 
              className="btn-contact btn-desktop"
              linkProps={{href: "/contato"}}
              label="Entre em contato"
            />

          </section>
        </section>
      </DefaultContent>
    )
  }

  return (
    <Style>
      <UpperSection
        navbar={{
          className: "about"
        }}
        head={{
          title: "progAmando - Sobre", 
          description: "Conheça um pouco mais sobre Giovanni Pregnolato Rosim, o criador do progAmando!",
          previewImage: '/assets/images/me.png',
        }}
        bannerHero={{
          Component: BannerHeroFull,
          props: {
            imageUrl: "/assets/images/banners/about.jpg",
            className: "about",
            children: content()
          }
        }}
      />
    </Style>
  )
}