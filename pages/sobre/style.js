import styled from 'styled-components'

import breakpoints from '../../theme/variables/breakpoints'

export default styled.div`
  background-color: var(--background);

  .about-container {
    display: grid;
    grid-gap: 2rem;
    height: 100%;
    align-items: center;
    align-items: flex-start;

    text-align: center;

  }

  .banner-hero.about {
    padding: 0;
  }

  .banner-hero .overlay {
    opacity: .9;
  }

  .about-infos .subtitle {
    font-size: 2.5rem;
  }

  .btn-contact {
    width: 30rem;
    margin-top: 2rem;
  }

  .about-me p {
    margin: 2.5rem 0;
    font-size: 1.6rem;
  }

  .about-photo img {
    border-radius: 50%;
    box-shadow: 1px 1px 3px 1px var(--title);
  }

  .about-photo {
    grid-row-end: -1;
  }

  .btn-contact.btn-desktop {
    display: none;
  }

  .btn-contact.btn-mobile {
    display: block;
    margin: 5rem auto;
  }

  @media (min-width: ${breakpoints.desktop}) {

    header .about .logo .bits {
      top: 4px;
    }

    .about-container {
      grid-template-columns: 1fr 1fr;
    }

    .about-photo {
      position: sticky;
      top: calc(var(--container-header-total-height) + 1rem);
      grid-row-end: auto;
    }

    .btn-contact.btn-desktop {
      display: block;
    }

    .btn-contact.btn-mobile {
      display: none;
    }

    img {
      position: sticky;
      top: 2rem;
    }

  }

`