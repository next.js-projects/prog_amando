import styled from 'styled-components'

export default styled.div`
  background-color: var(--background);

  button {
    background-color: var(--primary);
    color: var(--secundary);
  }

  .banner-hero:before {
    content: none;
  }

  .banner-hero {
    height: 50vh;
    background-position: center;
  }

  h1.title {
    color: var(--title);
    font-size: 2.5rem;
    text-align: center;
    margin: 5rem 0;
  }

  .share-buttons--container {
    margin-top: 3rem;
    text-align: center;
  }

  .share-buttons {
    display: flex;
    justify-content: center;
  }

  .share-buttons button {
    margin: 1rem;
    outline: none;
    height: 3.6rem;
    width: 3.6rem;

    border: none;
    border-radius: 50%;
    background: none;

    cursor: pointer;
  }

  .share-buttons button svg {
    border-radius: 50%;
    height: 3.6rem;
    width: 3.6rem;

    transition: .3s;
  }

  .bnt-share_copy-link svg {
    background-color: white;
    padding: 3px;
    background: var(--primary);
    color: white;
  }

  .share-buttons button:hover {
    svg {
      height: 4rem;
      width: 4rem;
    }
  }

  p {
    margin: 2rem 0;
    line-height: 2.5rem;
    font-size: 1.6rem;
  }

  p.image {
    text-align: center;
  }

  p a {
    color: var(--primary-bold);
    font-weight: 700;
  }

  p .link, p .comment {
    color: var(--secundary);
  }

  p .link {
    font-size: .9em;
  }

  p b {
    color: var(--primary-bold);
  }

  p .subtitle {
    color: var(--subtitle);
    font-weight: 700;
  }
`