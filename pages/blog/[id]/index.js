import { useState, useCallback, useEffect, useMemo } from 'react'
import { useRouter } from 'next/router'

import htmlToReact from 'html-react-parser'

import Style from './style'

// Contenful
import { createClient } from "contentful"
import config from "../../../config.json"

// https://github.com/contentful/rich-text/tree/master/packages/rich-text-react-renderer
import { documentToReactComponents } from '@contentful/rich-text-react-renderer'
import { BLOCKS } from '@contentful/rich-text-types'


// Components
import UpperSection from '../../../components/upperSection'
import BannerHero from '../../../components/banner-hero'
import DefaultContent from '../../../components/default-content'

import { BsLink } from 'react-icons/bs'
import {CopyToClipboard} from 'react-copy-to-clipboard'

// https://www.npmjs.com/package/react-alert
import { useAlert } from "react-alert"

// Share component
import {
  EmailShareButton,
  FacebookShareButton,
  FacebookMessengerShareButton,
  InstapaperShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  WorkplaceShareButton,
} from "react-share"

import {
  EmailIcon,
  FacebookIcon,
  FacebookMessengerIcon,
  InstapaperIcon,
  LinkedinIcon,
  TwitterIcon,
  WhatsappIcon,
  WorkplaceIcon,
} from "react-share"


// Instantiate the app client
const client = createClient({
  space: config.space,
  accessToken: config.accessToken
})

const Button = (props) => <CopyToClipboard text={props.url}><button {...props}/></CopyToClipboard>

const Post = (props) => {

  const alert = useAlert()

  const shareIcons = [
    {Button: WhatsappShareButton, Icon: WhatsappIcon},
    {Button: LinkedinShareButton, Icon: LinkedinIcon},
    {Button: FacebookMessengerShareButton, Icon: FacebookMessengerIcon},
    {Button: FacebookShareButton, Icon: FacebookIcon},
    {Button: EmailShareButton, Icon: EmailIcon},
    {Button: Button, Icon: BsLink, props: {
      className: "bnt-share_copy-link", 
      onClick: () => alert.success("Link copiado!")
    }},
    // {Button: InstapaperShareButton, Icon: InstapaperIcon},
    // {Button: TwitterShareButton, Icon: TwitterIcon},
  ]

  const [post, setPost] = useState(props.postData)

  const router = useRouter()

  // const getPost = useCallback((slug) => {
  //   client.getEntries({
  //     content_type: "progAmandoArticle",
  //     "fields.slug": slug,
  //     order: "-sys.createdAt"
  //   }).then(entries => setPost(entries.items[0]))
  // })

  // useEffect(() => {
  //   getPost(router.query.id || window.location.pathname.replace('/blog/', ''))
  // }, [])

  const options = {
    // renderMark: {
    //   [MARKS.BOLD]: text => <Bold>{text}</Bold>,
    // },
    renderNode: {
      [BLOCKS.EMBEDDED_ASSET]: (node) => {
        const { title, file } = node.data.target.fields;
        return <p className="image"><img src={file.url} title={title} alt={title} /></p>
      },
      [BLOCKS.PARAGRAPH]: (node, children) => 
        // children.map((c,i) => <p key={'paragraph_' + i}>{typeof c == "string" ?  htmlToReact(c) : c}</p>)
        <p>{children.map((c,i) => typeof c == "string" ?  htmlToReact(c) : c)}</p>
      ,
    }
  };

  const createPost = useCallback(() => {
    if(!post) return

    const shareButtonsContainer = (label) => {
      return (
        <section className="share-buttons--container">
        <label>{label || "Compartilhe..."}</label>
        <div className="share-buttons">
          {shareIcons.map(({Button, Icon, props}, i) => 
            <Button key={i} 
              url={`https://prog-amando.vercel.app/blog/${post.fields.slug}`} 
              {...props}>
                <Icon />
            </Button>)
          }
            {/* <Button key={i} url={window.location.href} {...props}><Icon /></Button>)} */}
        </div>
        </section>
      )
    }

    return (
      <>
        <h1 className="title">{post.fields.title}</h1>
        {shareButtonsContainer()}
        <article className={post.fields.slug}>
          {documentToReactComponents(post.fields.content, options)}
        </article>
        {shareButtonsContainer("Gostou do conteúdo? Compartilhe nas suas redes:")}
      </>
    )
  })

  const bannerParams = useMemo(() => {
    return ({
      Component: BannerHero,
      props: {
        imageUrl: post && post.fields.image.fields.file.url,
      }
    })
  }, [post])

  return (
    <Style>
      { post && 
        <UpperSection 
          head={{
            title: `progAmando - ${post.fields.title}`, 
            description: `Tenha mais informações sobre a postagem: "${post.fields.title}".`,
            previewImage: post.fields.image.fields.file.url,
          }}
          bannerHero={bannerParams} 
        /> 
      }

      <DefaultContent className={`blog-post `}
        headerTxt={`<article id='${post && post.fields.slug}' class='blog-post'>`}
        footerTxt="</article>"
        isLoading={!post}
      >
        {createPost()}
      </DefaultContent>
    </Style>
  )
}

// Referencia de estudo (Rocketseat)
// https://www.youtube.com/watch?v=u1kCtkVR7cE&t=954s&ab_channel=Rocketseat

export async function getStaticProps({ params }) {
  // Fetch necessary data for the blog post using params.id
  const entries = await client.getEntries({
    content_type: "progAmandoArticle",
    "fields.slug": params.id,
    order: "-sys.createdAt"
  })//.then(entries => setPost(entries.items[0]))

  return {
    props: {
      postData: entries.items[0],
    },
    // tempo em segundos onde a página estática será refeita automaticamente e salva em cache
    // do servidor, sem depender de um novo build.
    revalidate: 10,
  }
}

export async function getStaticPaths() {
  // Call an external API endpoint to get posts
  const entries = await client.getEntries({
    content_type: "progAmandoArticle",
    select: ["fields.slug"],
    order: "-sys.createdAt"
  })

  const posts = entries.items

  // Get the paths we want to pre-render based on posts
  const paths = posts.map((post) => ({
    params: { id: post.fields.slug },
  }))

  // We'll pre-render only these paths at build time.

  // { fallback: false } means other routes should 404. Dessa forma, caso não encontre a
  // versão estática da página, retornará uma página 404.

  // { fallback: true } representa que caso seja chamada uma rota que ainda não existe
  // ele tentará criar sua versão estatica de maneira dinâmica conforme as requisições.
  // * Caso de uso: na geração das páginas estáticas dos tickects da Rocketseat para o 
  // Next Level Week 3.0, pois foram milhares de pessoas que se increveram e criar todas essas
  // páginas a cada build faria o processo ficar extremamente custoso.
  // * Como saber que é uma página que ainda não é estática?
  // Usando o isFallback do useRouter do next/link.
  // Caso, isFallback for verdadeiro, ele ainda estará carregando a página estática 
  return { paths, fallback: true }
}

export default Post
