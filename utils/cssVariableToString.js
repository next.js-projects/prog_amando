export const getColorString = (name) => {
  const rootStyle = getComputedStyle(document.querySelector(':root'))
  return rootStyle.getPropertyValue(name)
}

