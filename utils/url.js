export const getLastPartofUrl = () => {
  const match = window.location.pathname.match(/[^\/]+$/)
  return match && match[0]
}